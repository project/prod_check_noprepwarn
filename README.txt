README.txt
----------
This (mini) module extends the Production check & Production monitor by a simple 
warning message functionality. If CSS or JS compression is disabled, 
the warning shows up for all users with the "administer site configuration"-permission.

The module has been created because we again and again ran into the problem 
that someone forgot to activate the JS / CSS preprocessing // compression on live pages.
The result was, that the theme was not displayed correctly in the Internet Explorer 
because of this (IE) Bug: #228818: IE: Stylesheets ignored after 31 link/style tags 
(http://drupal.org/node/228818).

If you ran into the same problem, this might be your module!

DEPENDENCIES
------------
- Production check & Production monitor (http://drupal.org/project/prod_check)

SUPPORTED MODULES
-----------------
- advagg (Checks if advagg JS/CSS compression instead of Drupal built-in permission)

INSTALLATION
------------
- Copy prod_check_noprep_warn directory to /sites/all/modules
- Enable module at /admin/build/modules
- That's it! No configuration needed. Give if a try if you like to by disabling JS or CSS compression.

AUTHOR/MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Development sponsored by:
    webks: websolutions kept simple (http://www.webks.de)
      and
    DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (http://www.drowl.de)